<?php
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Input</title>
    <style>

#InformationHolder {
width: 750px;
            margin: 50px auto;
            padding:25px;
            background: #DDDDDD;
            color: #000fff;
            font-size: 15px;
            font-weight: normal;
            line-height: 20px;
            letter-spacing: .5px;
            box-shadow: 5px 5px 5px rgba(0,0,0,.3);
            font-family: Arial;
        }
        p {
    margin: 0px;
        }
        #pageTitle {
            padding:15px 0;
        }
        #pageTitle h1 {
            padding: 0;
            margin: 0;
            text-align: center;
            font-size: 24px;
            font-weight: normal;
            line-height: 30px;
            letter-spacing: 1px;
            text-transform: uppercase;
            font-family: "Arial Black";
        }
        #stdInfo {
            padding: 30px 0;
        }
        #stdInfo table td {
            padding: 5px;
        }
        #InputMark {
            padding: 0 0 15px;
        }
        #InputMark table td {
            padding: 5px;
        }
        #submit {
            padding: 15px 0;
        }
        #submit input {
            display: block;
            text-transform: uppercase;
            width: 250px;
            height: 50px;