<?php
namespace App;
require_once('Person.php');


class Student extends Person
{

    protected $studentID,  $banglaMark, $englishMark, $mathMark, $ICTmark;


    public function setData($postArray){

        if(array_key_exists("SEID",$postArray))    $this->studentID = $postArray['SEID'];
        if(array_key_exists("Name",$postArray))    $this->name = $postArray['Name'];
        if(array_key_exists("DOB",$postArray))    $this->dob = $postArray['DOB'];
        if(array_key_exists("Gender",$postArray))    $this->gender = $postArray['Gender'];
        if(array_key_exists("BanglaMark",$postArray))    $this->banglaMark = $postArray['BanglaMark'];
        if(array_key_exists("EnglishMark",$postArray))    $this->englishMark = $postArray['EnglishMark'];
        if(array_key_exists("MathMark",$postArray))    $this->mathMark = $postArray['MathMark'];
        if(array_key_exists("ICTMark",$postArray))    $this->ICTmark = $postArray['ICTMark'];


    }



    public function getData(){

        echo "Student ID: " . $this->studentID . "<br>";
        echo "Student Name: " . $this->name . "<br>";
        echo "Student DOB: " . $this->dob . "<br>";
        echo "Student Gender: " . $this->gender . "<hr>";

        echo "Bangla Mark: " . $this->banglaMark . "<br>";
        echo "English Mark: " . $this->englishMark . "<br>";
        echo "Math Mark: " . $this->mathMark . "<br>";
        echo "ICT Mark: " . $this->ICTmark. "<br>";

    }




}

